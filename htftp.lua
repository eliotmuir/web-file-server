local SORT_MAP = {
	name_asc = "",
   name_des = "-r",
   time_asc = "-rt",
   time_des = "-t",
   size_asc = "-rS",
   size_des = "-S"
}

local function list_dir(Dir, Sort)
   local sort = Sort and SORT_MAP[Sort] or SORT_MAP.name_asc
   local fd = io.popen("ls " .. sort .. " " .. Dir)
   local stdout = fd:read("*a")
   fd:close()
   return stdout:trimWS():split("\n")
end

-- basic and likely buggy about root check
local function above_root(Path)
   assert(Path:sub(1,1) == "/")
	local dirs = Path:sub(2):split("/")
   local parent_count = 0
   local dot_dot_count = 0
   for _,v in ipairs(dirs) do
      if v == ".." then
         dot_dot_count = dot_dot_count + 1
         if dot_dot_count > parent_count then
            return true
         end
      elseif v ~= "." then
         parent_count = parent_count + 1
      end
      trace(dot_dot_count, parent_count)
   end
   return false
end

local dir_header = '<a href="?sort=%s">Name</a>' .. (" "):rep(60) .. '<a href="?sort=%s">Time</a>' .. (" "):rep(22) .. '<a href="?sort=%s">Size</a>'
local function dir_html_header(Sort)
   if not Sort then Sort = "name_asc" end
   local name_sort = Sort == "name_asc" and "name_des" or "name_asc"
   local time_sort = Sort == "time_asc" and "time_des" or "time_asc"
   local size_sort = Sort == "size_asc" and "size_des" or "size_asc"
   return string.format(dir_header, name_sort, time_sort, size_sort)
end

local downloadable = {
   exe = true,
   dmg = true,
   tar = true,
   gz  = true,
   zip = true,
}

local dir_body = "<body><h1>Index of %s</h1><pre>%s<hr>%s</pre><hr></body>"
local function dir_html(Path, Sort, RootDir)
   local full_path = RootDir .. Path
   -- TODO: basic, and likely unsafe way to assume file request
   if Path:sub(-1) ~= "/" then
      local fd = io.open(full_path)
      if not fd then
         return { code = 404 }
      end
      local content = fd:read("*a")
      fd:close()
      local header = {}
      local ext = Path:match(".*%.(%w+)")
      if ext and downloadable[ext] then
         header['Content-Type'] = "application/octet-stream"
         header['Content-Disposition'] = 'attachment; filename="' .. Path:match(".*/(.+)") .. '"'
      else
         header['Content-Type'] = "text/plain"
      end
      return { body=content, headers=header }
   end

   local rows = {}
   local dir_content = list_dir(full_path, Sort)
   for i,v in ipairs(dir_content) do
      if #v > 0 then
         local stats = os.fs.stat(full_path .. v)
         local filename = stats.isdir and v .. "/" or v
         local padding  = (" "):rep(64 - #filename)
         local filetime = os.date("%b-%d-%Y %H:%M:%S", stats.mtime)
         local filesize = stats.size .. " B"
         rows[i] = string.format("<a href=\"%s\">%s</a>%s%s% 10s",
            filename, filename, padding, filetime, filesize)
      end
   end
   if Path ~= "/" then
      -- insert parent dir link for non-root dirs
      table.insert(rows, 1, "<a href=\"../\">../</a>")
   end
   local content = string.format(dir_body, Path, dir_html_header(Sort), table.concat(rows, "\n"))
   return { body=content, headers={['Content-Type']="text/html"} }
end

return {
   dir_html = dir_html
}